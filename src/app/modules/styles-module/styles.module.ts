import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatButtonModule} from '@angular/material/button';
import {MatInputModule} from '@angular/material/input';
import {MatListModule} from '@angular/material/list';
import {MatBadgeModule} from '@angular/material/badge';
import {MatChipsModule} from '@angular/material/chips';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatCardModule} from '@angular/material/card';
import {MatTooltipModule} from '@angular/material/tooltip';


const materialModules = [
	BrowserAnimationsModule,
	MatButtonModule,
	MatInputModule,
	MatListModule,
	MatBadgeModule,
	MatChipsModule,
	MatToolbarModule,
	MatFormFieldModule,
	MatCardModule,
	MatTooltipModule
];

@NgModule({
	imports: [
		CommonModule,
		...materialModules
	],
	exports: [
		...materialModules
	],
	declarations: []
})
export class StylesModule {}

import { Injectable } from '@angular/core';
import {Observable, Observer} from 'rxjs';

import * as socketIo from 'socket.io-client';
import { Message } from '../models/message';


// const SERVER_URL = 'http://localhost:8080';
const SERVER_URL = 'http://ec2-54-86-195-8.compute-1.amazonaws.com:8080';

@Injectable({providedIn: 'root'}) export class SocketService {
	private socket;
	constructor() {}

	public initSocket(): void {
		this.socket = socketIo(
			SERVER_URL,
			{transports: ['websocket']}
		);
	}
	public sendMessage(msg: Message): void {
		this.socket.emit('message', msg);
	}
	public onMessage(): Observable<Message> {
		return new Observable<Message>(obsr =>
			this.socket.on('message', (data: Message) => obsr.next(data))
		);
	}
	public onEvent(event: string): Observable<any> {
		return new Observable<any>(obsr =>
			this.socket.on(event, () => obsr.next())
		);
    }
}

import {Component, OnInit, HostListener} from '@angular/core';
import {v4 as uuid} from 'uuid';

import {SocketService} from '../../services/socket.service';
import {Message} from '../../models/message';


@Component({
	selector: 'p2p-chat-main',
	templateUrl: './main.component.html',
	styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {
	private ioConnection: any;
	public userID: string = '';
	public content: string = '';
	public messages: Message[] = [];

	constructor(private socketService: SocketService) {}
	ngOnInit() {
		this.userID = uuid();
		this.initIoConnection();
		this.sendMessage(1);
	}
	@HostListener('window:beforeunload') onPageUnload() {
		this.sendMessage(0);
	}
	private initIoConnection(): void {
		this.socketService.initSocket();
		this.ioConnection = this.socketService.onMessage()
		  	.subscribe((message: Message) => this.messages.push(message));
		this.socketService.onEvent('connect')
		 	.subscribe(() => console.log('Connected to the socketIO server.'));
		this.socketService.onEvent('disconnect')
		 	.subscribe(() => console.warn('Disconnected from the socketIO server.'));
	}

	public sendMessage(op: number = 2): void {
		if(op === 2) {
			if(!this.content || !this.content.trim() || this.content.length > 160) return;
			this.socketService.sendMessage({
				userID: this.userID,
				content: this.content.trim(),
				action: 'Message',
				timestamp: new Date().getTime()
			});
			this.content = '';
		}
		if(op === 1) {
			this.socketService.sendMessage({
				userID: this.userID,
				action: 'Connected',
				timestamp: new Date().getTime()
			});
		}
		if(op === 0) {
			this.socketService.sendMessage({
				userID: this.userID,
				action: 'Disconnected',
				timestamp: new Date().getTime()
			});
		}
	}

	public getMessageTimeStr(timestamp: number): string {
		return `${new Date(timestamp).toLocaleDateString()} - ${new Date(timestamp).toLocaleTimeString()}`;
	}
}

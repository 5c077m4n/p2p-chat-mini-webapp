import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms';

import {StylesModule} from './modules/styles-module/styles.module';
import { AppComponent } from './app.component';
import { MainComponent } from './components/main/main.component';

@NgModule({
  declarations: [
		AppComponent,
		MainComponent
	],
	imports: [
		BrowserModule,
		FormsModule,
		StylesModule
	],
	exports: [
		StylesModule
	],
	providers: [],
	bootstrap: [AppComponent]
})
export class AppModule {}

export interface Message {
	userID?: string;
	content?: string;
	action?: string;
	group?: string;
	timestamp?: number;
}
